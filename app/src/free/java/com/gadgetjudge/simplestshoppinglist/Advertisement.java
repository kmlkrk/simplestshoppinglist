package com.gadgetjudge.simplestshoppinglist;

import android.content.Context;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

/**
 * Created by kamil.paluch on 2/10/17.
 */

public class Advertisement {

    public Advertisement(Context context, String adId, View adViewRaw) {

        final AdView adView = (AdView) (adViewRaw);
        MobileAds.initialize(context, adId);
        AdRequest.Builder adRequest = new AdRequest.Builder();
        adRequest.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        adRequest.addTestDevice("B7951073F551FC4BDF39F5A32E7502FA");
        adView.loadAd(adRequest.build());

        //hide the ad when it can't be loaded
//        adView.setAdListener(new AdListener() {
//            @Override
//            public void onAdFailedToLoad(int errorCode) {
//                adView.setVisibility(View.GONE); //hide ad container
//            }
//        });
    }
}
