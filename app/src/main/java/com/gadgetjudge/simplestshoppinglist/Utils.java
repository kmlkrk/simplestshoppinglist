package com.gadgetjudge.simplestshoppinglist;

import android.app.Activity;
import android.preference.PreferenceManager;

/**
 * Created by KaMeL on 7/20/2017.
 */

public class Utils {
    public static void onActivityCreateSetTheme(Activity caller) {
        boolean useDarkTheme = PreferenceManager.getDefaultSharedPreferences(caller.getApplicationContext()).getBoolean("theme_preference_switch", false);

        if (useDarkTheme) {
            caller.setTheme(R.style.AppThemeDark);
        }
    }
}
