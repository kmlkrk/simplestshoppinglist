package com.gadgetjudge.simplestshoppinglist;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gadgetjudge.simplestshoppinglist.helper.ItemTouchHelperAdapter;
import com.gadgetjudge.simplestshoppinglist.helper.ItemTouchHelperViewHolder;
import com.gadgetjudge.simplestshoppinglist.helper.OnStartDragListener;

import java.util.ArrayList;
import java.util.Collections;

public class ShoppingItemAdapter extends RecyclerView.Adapter<ShoppingItemAdapter.ItemViewHolder> implements ItemTouchHelperAdapter {
    private static final int CONTENT_TYPE = 0;
    private static boolean AD_INJECTED = false;
    private static final int AD_TYPE = 1;
    private final Context context;
    private final ArrayList<ShoppingItem> itemsInList;
    private final OnStartDragListener dragStartListener;

    public ShoppingItemAdapter(Context context, OnStartDragListener dragStartListener, ArrayList<ShoppingItem> itemsInListArray) {
        this.context = context;
        this.dragStartListener = dragStartListener;
        this.itemsInList = itemsInListArray;
        this.itemsInList.add(null); /* for ad */
    }

    @Override
    public int getItemViewType(int position) {
        if (position + 1 == itemsInList.size()) {
            return AD_TYPE;
        } else {
            return CONTENT_TYPE;
        }
    }

    @Override
    public @NonNull ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == CONTENT_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_view_item, parent, false);
            return new ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ad_view, parent, false);
            return new ItemViewHolder(view);
        }

    }

    @Override
    public @NonNull void onBindViewHolder(final @NonNull ItemViewHolder holder, final int position) {
        if (holder.getItemViewType() == CONTENT_TYPE) {
            final ShoppingItem shoppingItem = this.itemsInList.get(position);

            // 4. Bind the data to the ViewHolder
            if (shoppingItem != null) {
                holder.shoppingItemName.setText(shoppingItem.getName());

                // styles for items in cart
                if (shoppingItem.getInCart() == 1) {
                    holder.shoppingItemName.setAlpha(0.4f);
                    holder.shoppingItemName.setPaintFlags(holder.shoppingItemName.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//                    holder.itemView.setBackgroundColor(ContextCompat.getColor(context, R.color.transparent_black_30));
                    holder.shoppingItemRemoveButton.setVisibility(View.INVISIBLE);
                    holder.shoppingItemEditButton.setVisibility(View.INVISIBLE);
                    holder.shoppingItemSortHandle.setVisibility(View.INVISIBLE);
                    holder.shoppingItemCartButton.setImageResource(R.drawable.cart_button_remove);
                } else {
                    // styles for items not in cart
                    holder.shoppingItemName.setAlpha(1);
                    holder.shoppingItemName.setPaintFlags(0);
//                    holder.itemView.setBackgroundColor(0);
                    holder.shoppingItemRemoveButton.setVisibility(View.VISIBLE);
                    holder.shoppingItemEditButton.setVisibility(View.VISIBLE);
                    holder.shoppingItemSortHandle.setVisibility(View.VISIBLE);
                    holder.shoppingItemCartButton.setImageResource(R.drawable.cart_button_add);
                }

                // Start a drag whenever the handle view it touched
                holder.shoppingItemSortHandle.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                            dragStartListener.onStartDrag(holder);
                        }
                        return false;
                    }
                });

                // remove button
                holder.shoppingItemRemoveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent removeSingleItemIntent = new Intent();
                        removeSingleItemIntent.setAction("com.gadgetjudge.simplestshoppinglist.REMOVE_SINGLE_ITEM");
                        removeSingleItemIntent.putExtra("item_position", holder.getAdapterPosition());
                        context.sendBroadcast(removeSingleItemIntent);
                    }
                });

                holder.shoppingItemEditButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent editSingleItemIntent = new Intent();
                        editSingleItemIntent.setAction("com.gadgetjudge.simplestshoppinglist.EDIT_SINGLE_ITEM");
                        editSingleItemIntent.putExtra("item_position", holder.getAdapterPosition());
                        context.sendBroadcast(editSingleItemIntent);
                    }
                });

                holder.shoppingItemCartButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent cartSingleItemIntent = new Intent();
                        cartSingleItemIntent.setAction("com.gadgetjudge.simplestshoppinglist.CART_SINGLE_ITEM");
                        cartSingleItemIntent.putExtra("item_position", holder.getAdapterPosition());
                        context.sendBroadcast(cartSingleItemIntent);
                    }
                });

                holder.shoppingItemName.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent cartSingleItemIntent = new Intent();
                        cartSingleItemIntent.setAction("com.gadgetjudge.simplestshoppinglist.CART_SINGLE_ITEM");
                        cartSingleItemIntent.putExtra("item_position", holder.getAdapterPosition());
                        context.sendBroadcast(cartSingleItemIntent);
                    }
                });
            }
        } else {
            // AD VIEW
            if (BuildConfig.FLAVOR.toLowerCase().equals("free")) {
                if (!AD_INJECTED) {
                    View adViewBelowListRaw = holder.itemView.findViewById(R.id.adViewBelowList);
                    new Advertisement(context, "ca-app-pub-8060400006725241/6310394693", adViewBelowListRaw);
                    AD_INJECTED = true;
                }
            }
        }
    }

    @Override
    public void onItemDismiss(int position) {
        itemsInList.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {

        // If items already in cart
        if (itemsInList.get(fromPosition).getInCart() == 1) {
            if (itemsInList.get(toPosition).getInCart() != 1) {
                // DO NOT allow it to be moved above first item in cart
                return false;
            }
        } else if (itemsInList.get(toPosition).getInCart() == 1) {
            // DO NOT allow to move items below first item in cart
            toPosition = fromPosition;
        }

        Collections.swap(itemsInList, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);

        return true;
    }

    @Override
    public int getItemCount() {
        return itemsInList.size();
    }

    public ShoppingItem getItem(Integer index){
        return itemsInList.get(index);
    }

    /**
     * Simple example of a view holder that implements {@link ItemTouchHelperViewHolder} and has a
     * "handle" view that initiates a drag event when touched.
     */
    public class ItemViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {

        private final TextView shoppingItemName;
        private final ImageButton shoppingItemSortHandle;
        private final ImageButton shoppingItemRemoveButton;
        private final ImageButton shoppingItemEditButton;
        private final ImageButton shoppingItemCartButton;

        private ItemViewHolder(View itemView) {
            super(itemView);
            this.shoppingItemName            = itemView.findViewById(R.id.textViewItemName);
            this.shoppingItemSortHandle      = itemView.findViewById(R.id.imageButtonDragHandle);
            this.shoppingItemRemoveButton    = itemView.findViewById(R.id.imageButtonRemove);
            this.shoppingItemEditButton      = itemView.findViewById(R.id.imageButtonEdit);
            this.shoppingItemCartButton      = itemView.findViewById(R.id.imageButtonInCart);
        }

        @Override
        public void onItemSelected() {
//            itemView.setBackgroundColor(context.getResources().getColor(R.color.primary));
        }

        @Override
        public void onItemClear() {

        }

    }


}