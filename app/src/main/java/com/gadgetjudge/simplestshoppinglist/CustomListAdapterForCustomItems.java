package com.gadgetjudge.simplestshoppinglist;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

public class CustomListAdapterForCustomItems extends ArrayAdapter<CustomItem> {

    private final Context context;
    private final ArrayList<CustomItem> customItemsList;
    private ShoppingListManager slm;


    public CustomListAdapterForCustomItems(Activity context, ArrayList<CustomItem> customItemsList) {
        super(context, R.layout.list_view_item, customItemsList);

        this.context            = context;
        this.customItemsList    = customItemsList;

        slm = new ShoppingListManager(context);
    }

    @NonNull
    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.list_view_customize_item, parent, false);

        TextView shoppingItemNameTextView       = rowView.findViewById(R.id.textViewItemName);
        ImageButton shoppingItemRemoveButton    = rowView.findViewById(R.id.imageButtonRemove);
        ImageButton shoppingItemEditButton      = rowView.findViewById(R.id.imageButtonEdit);

        shoppingItemNameTextView.setText(customItemsList.get(position).getName());

        //set on click listeners for buttons
        shoppingItemRemoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent removeSingleItemIntent = new Intent();
                removeSingleItemIntent.setAction("com.gadgetjudge.simplestshoppinglist.REMOVE_SINGLE_ITEM");
                removeSingleItemIntent.putExtra("item_id", customItemsList.get(position).getId());
                context.sendBroadcast(removeSingleItemIntent);
            }
        });

        shoppingItemEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editSingleItemIntent = new Intent();
                editSingleItemIntent.setAction("com.gadgetjudge.simplestshoppinglist.EDIT_SINGLE_ITEM");
                editSingleItemIntent.putExtra("item_id", customItemsList.get(position).getId());
                context.sendBroadcast(editSingleItemIntent);
            }
        });

        return rowView;
    }
}
