package com.gadgetjudge.simplestshoppinglist;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class ActivityCustomItems extends AppCompatActivity {
	private CoordinatorLayout coordinatorLayout;
	private AutoCompleteTextView autoCompleteTextViewCustomItemName;
    private ImageButton imageButtonAdd;
    private ListView listViewItems;
    private ArrayList<CustomItem> allPredefinedItems;
    private ArrayList<CustomItem> temporaryItems;
    private CustomItem temporaryItem;
	private int temporaryItemIndexInList;
    private CustomListAdapterForCustomItems itemsInListAdapter;
    private ShoppingListManager slm;
    private Context applicationContext;
    private String removeSingleCustomItemIntentAction;
    private String editSingleCustomItemIntentAction;
    private IntentFilter removeSingleCustomItemIntentFilter;
    private IntentFilter editSingleCustomItemIntentFilter;
    private BroadcastReceiver customActionsReceiver;
	private LinearLayout doneButtonContainer;
    private int toastOffset;
    private String[] predefinedItemsArray;
	private String[] allAvailableItemNames;
	private Button doneManagingCustomItemsButton;
    private Snackbar undoSnackbar;
    private int snackbarDuration;
    private View.OnClickListener undoRemoveSingleItemClickListener;
    private View.OnClickListener undoRemoveAllItemsClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this);
        setContentView(R.layout.activity_edit_custom_items);

        /** ASSIGN VALUES TO OBJECTS AND VARS */
		coordinatorLayout					= findViewById(R.id.coordinatorLayout);
		/* using autocomplete because I had trouble gaining focus for regular EditText after using soft keyboard to save item */
		autoCompleteTextViewCustomItemName	= findViewById(R.id.autoCompleteTextViewCustomItemName);
        imageButtonAdd						= findViewById(R.id.imageButtonAdd);
        listViewItems						= findViewById(R.id.listViewItems);
		doneButtonContainer					= findViewById(R.id.doneButtonContainer);
		doneManagingCustomItemsButton		= findViewById(R.id.buttonDoneManagingCustomItems);
        applicationContext					= getApplicationContext();
        slm									= new ShoppingListManager(applicationContext);
        allPredefinedItems = slm.getAllCustomItemsFromDB();
        toastOffset							= getResources().getDimensionPixelSize(R.dimen.m_spacing);
        itemsInListAdapter					= new CustomListAdapterForCustomItems(this, allPredefinedItems);
		removeSingleCustomItemIntentAction	= "com.gadgetjudge.simplestshoppinglist.REMOVE_SINGLE_ITEM";
        editSingleCustomItemIntentAction	= "com.gadgetjudge.simplestshoppinglist.EDIT_SINGLE_ITEM";
        removeSingleCustomItemIntentFilter	= new IntentFilter(removeSingleCustomItemIntentAction);
        editSingleCustomItemIntentFilter	= new IntentFilter(editSingleCustomItemIntentAction);
        predefinedItemsArray				= getResources().getStringArray(R.array.shopping_items);
        snackbarDuration                    = 5000;//5seconds

		createListOfAvailableItems();

        //set up broadcast receivers
        customActionsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
			if (intent.getExtras() != null) {
				Bundle intentExtras = intent.getExtras();

				int itemId = intentExtras.getInt("item_id");

				//ACTION FOR REMOVE SINGLE ITEM
				if (intent.getAction().equals(removeSingleCustomItemIntentAction)) {
					removeSingleItem(itemId);
				}
				//ACTION FOR EDIT SINGLE ITEM
				else if (intent.getAction().equals(editSingleCustomItemIntentAction)) {
					editSingleItem(itemId);
				}
			}
            }
        };

        //enter key or center button on dpad will do the same as ok button
        autoCompleteTextViewCustomItemName.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
			if (event.getAction() == KeyEvent.ACTION_DOWN)
			{
				switch (keyCode)
				{
					case KeyEvent.KEYCODE_DPAD_CENTER:
					case KeyEvent.KEYCODE_ENTER:
						addItemToTheList();
						return true;
					default:
						break;
				}
			}
			return false;
            }
        });


        //assign adapter to listview
        listViewItems.setAdapter(itemsInListAdapter);

        /** if cart empty */
        addEmptyListMessage();

        /** SET ON CLICK LISTENERS */
        undoRemoveSingleItemClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
				undoRemoveSingleItem();
            }
        };

        undoRemoveAllItemsClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
				undoRemoveAllItems();
            }
        };

        //add button click listener
        imageButtonAdd.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addItemToTheList();
			}
		});

        //add button click listener
        doneManagingCustomItemsButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				backToMainActivity();
			}
		});

    }

    private void showUndoSnackbar(String text, int duration, String actionText, View.OnClickListener listener) {
        undoSnackbar = Snackbar.make(coordinatorLayout, text, duration);
        undoSnackbar.setAction(actionText, listener);
        undoSnackbar.setActionTextColor(ContextCompat.getColor(applicationContext, R.color.accent));

        View undoDeleteSnackView = undoSnackbar.getView();
        TextView textView = undoDeleteSnackView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setPadding(4, 0, 0, 0);

        undoSnackbar.show();
    }

	private void createListOfAvailableItems() {
		ArrayList<CustomItem> customItems			= slm.getAllCustomItemsFromDB();
		String[] customItemsArray					= new String[customItems.size()];

		//fill the array with all custom created items
		int i = 0;
		for (CustomItem ci : customItems) {
			customItemsArray[i] = ci.getName();
			i++;
		}

		//add predefined items and custom items to new array and use it to create
		allAvailableItemNames = new String[predefinedItemsArray.length + customItemsArray.length];
		allAvailableItemNames = slm.sortArrayAlphabetically(slm.combineArrays(predefinedItemsArray, customItemsArray));
	}

    private void addItemToTheList() {
        //make sure that text was entered
        if (!TextUtils.isEmpty(autoCompleteTextViewCustomItemName.getText())) {
			CustomItem latestItem = new CustomItem(autoCompleteTextViewCustomItemName.getText().toString().trim());

			//check if this items is not perhaps on the list of known items - this will help avoid duplicates
			if (!isInKnownItemsList(latestItem.getName())) {
				//retrieve id after saving in DB so that item in array has id which can be used for deleting,editing, etc
				latestItem.setId(slm.insertCustomItemIntoDB(latestItem));

				createListOfAvailableItems();

				itemsInListAdapter.add(latestItem);
				itemsInListAdapter.notifyDataSetChanged();

				showToast(latestItem.getName() + " " + getResources().getString(R.string.added_to_known_items));
			}
			else {
				showToast(latestItem.getName() + " " + getResources().getString(R.string.item_already_in_known_items_list));
			}

            autoCompleteTextViewCustomItemName.setText("");

        }
        else {
            showToast(getResources().getString(R.string.empty_input_error_message));
        }
    }

    private boolean isInKnownItemsList(String itemName) {
        boolean found = false;
        for (String existingItem: allAvailableItemNames) {
            if (existingItem.equals(itemName)) {
                found = true;
                break;
            }
        }

        return found;
    }


    private void undoRemoveSingleItem() {
		temporaryItem.setId(slm.insertCustomItemIntoDB(temporaryItem));

		try {
            itemsInListAdapter.insert(temporaryItem, temporaryItemIndexInList);
        } catch(Exception e) {
		    //do nothing
        }

		itemsInListAdapter.notifyDataSetChanged();

		showToast(temporaryItem.getName() + " " + getResources().getString(R.string.restored));
    }

    private void hideUndoSnackbar() {
        //snack might not have been created
        try {
            undoSnackbar.dismiss();
        }
        catch (Exception e) {
            //do nothing
        }
    }

    private void removeSingleItem(int itemId) {
        final CustomItem ci = slm.getCustomItemFromDB(itemId);

        temporaryItem = ci;
		temporaryItemIndexInList = findIndexOfElementInItemsListArray(itemId);

		removeSingleItemWorker(itemId);

		showUndoSnackbar(ci.getName() + " " + getResources().getString(R.string.removed_from_the_list), snackbarDuration, getResources().getString(R.string.undo), undoRemoveSingleItemClickListener);
    }

    private void removeSingleItemWorker(int itemId) {
		//removing item by passing object does not work because it is not the same object anymore, it is new copy from DB
		itemsInListAdapter.remove(itemsInListAdapter.getItem(findIndexOfElementInItemsListAdapter(itemId)));
		itemsInListAdapter.notifyDataSetChanged();

		//remove item from db
		slm.deleteCustomItem(itemId);
    }

	private int findIndexOfElementInItemsListArray(int itemId) {
		int index = -1;

		for (int i = 0; i < allPredefinedItems.size(); i++) {
			if (allPredefinedItems.get(i).getId() == itemId) {
				index = i;
				break;
			}
		}

		return index;
	}

	private int findIndexOfElementInItemsListAdapter(int itemId) {
		int index = -1;

		for (int i = 0; i < itemsInListAdapter.getCount(); i++) {
			if (itemsInListAdapter.getItem(i).getId() == itemId) {
				index = i;
				break;
			}
		}

		return index;
	}

    private void editSingleItem(final int itemId) {
        final CustomItem ci = slm.getCustomItemFromDB(itemId);

        //check if field is empty, if not ask user if they want to replace current text with edited item
        //otherwise place text in the field
        if (!TextUtils.isEmpty(autoCompleteTextViewCustomItemName.getText())) {
            //alert user that field not empty
            final AlertDialog.Builder promptDialog = new AlertDialog.Builder(this);
            promptDialog
                    //.setMessage(getResources().getString(R.string.item_name_not_empty_warning))
                    .setMessage("You already entered \'"+ autoCompleteTextViewCustomItemName.getText() +"\' but didn't save it yet. Do you want to save it before editing \'"+ ci.getName() +"\'")
                    .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            addItemToTheList();
                            editSingleItemWorker(itemId);
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            editSingleItemWorker(itemId);
                        }
                    }).create().show();
        }
        //input field was empty, we can proceed with editing
        else {
            editSingleItemWorker(itemId);
        }
    }

    private void editSingleItemWorker(int itemId) {
        CustomItem ci = slm.getCustomItemFromDB(itemId);
		autoCompleteTextViewCustomItemName.setText(ci.getName());

        //move cursor to the end of text
        autoCompleteTextViewCustomItemName.setSelection(autoCompleteTextViewCustomItemName.length());

		removeSingleItemWorker(itemId);

		//so that when I enter the same name without changing anything it won't tell me that item already exists because it doesn't and it would fail saving it to DB
		createListOfAvailableItems();
    }


    private void undoRemoveAllItems() {
        for (CustomItem ci : temporaryItems) {
			ci.setId(slm.insertCustomItemIntoDB(ci));
			itemsInListAdapter.add(ci);
        }
        itemsInListAdapter.notifyDataSetChanged();
    }

    private void removeAllCustomItems() {
        //check if list not empty
        if (allPredefinedItems.size() > 0) {

            temporaryItems = new ArrayList<>();
			temporaryItems.addAll(allPredefinedItems);

   			//regenerate list of items / clear list of all known items
			createListOfAvailableItems();

			itemsInListAdapter.clear();
			itemsInListAdapter.notifyDataSetChanged();

			//delete al items from db
			slm.deleteAllCustomItems();

			//show empty list message
			addEmptyListMessage();

			showUndoSnackbar(getResources().getString(R.string.items_removed), snackbarDuration, getResources().getString(R.string.undo), undoRemoveAllItemsClickListener);

        }
        //list is empty, display toast
        else {
            showToast(getResources().getString(R.string.no_items_in_list));
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity__custom_items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_remove_all_items) {
            removeAllCustomItems();
            return true;
        }
		else if (id == R.id.menu_back_to_shopping_list) {
			backToMainActivity();
			return true;
		}

        return super.onOptionsItemSelected(item);
    }

	private void backToMainActivity() {
		finish();
	}

    private void addEmptyListMessage() {
        //change listview's view if empty
        listViewItems.setEmptyView(findViewById(R.id.empty_list_view));
    }

    private void softRefreshListOfItems() {
        itemsInListAdapter.notifyDataSetChanged();
    }

    private void hardRefreshListOfItems() {
        itemsInListAdapter.clear();
        itemsInListAdapter.addAll(slm.getAllCustomItemsFromDB());
        itemsInListAdapter.notifyDataSetChanged();
    }

    private int findItemIndex(int itemId){
        int foundIndex = -1;

        for (int i = 0; i < allPredefinedItems.size(); i++) {
            if (itemId == allPredefinedItems.get(i).getId()) {
                foundIndex = i;
                break;
            }
        }
        return foundIndex;
    }

	private void showToast(String text) {
        try {
			hideUndoSnackbar();
        }
        catch (Exception e) {
            //do nothing
        }

		Toast toast = Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT);
		toast.show();
	}

    @Override
    protected void onResume() {
        super.onResume();

        this.registerReceiver(customActionsReceiver, removeSingleCustomItemIntentFilter);
        this.registerReceiver(customActionsReceiver, editSingleCustomItemIntentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();

        unregisterReceiver(customActionsReceiver);
    }

	@Override
	public void onBackPressed() {
		super.onBackPressed();
	}

}
