package com.gadgetjudge.simplestshoppinglist;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ShoppingListManager {
    private Context context;
    private DatabaseHandler dbh;
    private final String EXPORT_DB = "EXPORT_DB";
    private final String IMPORT_DB = "IMPORT_DB";
    private final String backupFileName = "SimplestShoppingListBackup";
    private final String backupFileLocation = "/SimplestShoppingList";
    private static final int REQUEST_WRITE_STORAGE = 112;
    private String permissionRequest;

    /**
     * CONSTRUCTOR
     */
    public ShoppingListManager(Context context) {
        this.context = context;
        this.dbh = DatabaseHandler.getInstance(context);
    }


    /**
     * PRIVATE FUNCTIONS
     */
    //TODO, figure out a way to test what would happen if user imports older version of a database
    private void saveDBfromSD(final Activity caller) {
        //Permission has been granted by user
        try {
            final File sd = Environment.getExternalStorageDirectory();
            final String path = sd + backupFileLocation;

            if (sd.canRead()) {
                final File data = Environment.getDataDirectory();
                final String dbName = DatabaseHandler.DATABASE_NAME;
                final String packageName = context.getPackageName();

                AlertDialog.Builder builder = new AlertDialog.Builder(caller);
                builder.setMessage("To import database please make sure that you put file named: \"" + backupFileName + "\" in this location:\n" + path + "\n\nAll existing items will be removed!")
                        .setTitle("Warning")
                        .setIcon(R.mipmap.ic_action_delete)
                        .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                try {
                                    String currentDBPath = "//data//" + packageName + "//databases//" + dbName;

                                    File currentDB = new File(data, currentDBPath);
                                    File backupDB = new File(path, backupFileName);

                                    FileChannel src = new FileInputStream(backupDB).getChannel();
                                    FileChannel dst = new FileOutputStream(currentDB).getChannel();

                                    //remove all existing items only when backup file was found
                                    deleteAllShoppingItems();

                                    dst.transferFrom(src, 0, src.size());
                                    src.close();
                                    dst.close();

                                    new AlertDialog.Builder(caller)
                                            .setMessage("Database has been imported")
                                            .setTitle("Import Successful")
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //recreate activity to show imported DB
//                                                    ActivityMain.itemsInListArray.addAll(getAllShoppingItemsFromDB());
//                                                    caller.recreate();

                                                    Intent importDatabaseIntent = new Intent();
                                                    importDatabaseIntent.setAction("com.gadgetjudge.simplestshoppinglist.IMPORT_DATABASE");
                                                    importDatabaseIntent.putExtra("item_position", 0);
                                                    context.sendBroadcast(importDatabaseIntent);
                                                }
                                            })
                                            .setCancelable(false).create().show();
                                } catch (IOException ioe) {
                                    String msg = "";
                                    if (ioe instanceof FileNotFoundException) {
                                        msg = " Backup file not found";
                                    }

                                    new AlertDialog.Builder(caller)
                                            .setMessage("Database has not been imported." + msg)
                                            .setTitle("Import Failed")
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //no need to do anything
                                                }
                                            }).create().show();
                                }
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //no need to do anything
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();

            } else {
                new AlertDialog.Builder(caller)
                        .setMessage("Can't read from storage")
                        .setTitle("Import Failed")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //no need to do anything
                            }
                        }).create().show();
            }
        } catch (Exception e) {
            new AlertDialog.Builder(caller)
                    .setMessage("Can't read file from storage or file not found")
                    .setTitle("Import Failed")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //no need to do anything
                        }
                    }).create().show();
        }
    }

    private void saveDBtoSD(final Activity caller) {
        //Permission has been granted by user
        try {
            final File sd = Environment.getExternalStorageDirectory();
            final File data = Environment.getDataDirectory();

            final String dbName = DatabaseHandler.DATABASE_NAME;
            final String packageName = context.getPackageName();

            if (sd.canWrite()) {
                AlertDialog.Builder builder = new AlertDialog.Builder(caller);
                builder.setMessage("If you already made a backup it will be replaced!")
                        .setTitle("Warning")
                        .setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String currentDBPath = "//data//" + packageName + "//databases//" + dbName;

                                File currentDB = new File(data, currentDBPath);
                                File path = new File(sd + backupFileLocation);
                                boolean dirCreated = path.mkdir();//variable only stored to make compiler stop showing warning
                                File backupDB = new File(path, backupFileName);

                                try {
                                    FileChannel src = new FileInputStream(currentDB).getChannel();
                                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                                    dst.transferFrom(src, 0, src.size());
                                    src.close();
                                    dst.close();

                                    new AlertDialog.Builder(caller)
                                            .setMessage("Exported to: " + backupDB.toString())
                                            .setTitle("Export Successful")
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //no need to do anything
                                                }
                                            }).create().show();
                                } catch (IOException ioe) {
                                    String msg;
                                    if (ioe instanceof FileNotFoundException) {
                                        msg = "There is nothing to export";
                                    } else {
                                        msg = "Can't save file to storage";
                                    }
                                    new AlertDialog.Builder(caller)
                                            .setMessage(msg)
                                            .setTitle("Export Failed")
                                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    //no need to do anything
                                                }
                                            }).create().show();
                                }

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //no need to do anything
                            }
                        });

                AlertDialog dialog = builder.create();
                dialog.show();

            } else {
                new AlertDialog.Builder(caller)
                        .setMessage("Can't save file to storage")
                        .setTitle("Export Failed")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //no need to do anything
                            }
                        }).create().show();
            }
        } catch (Exception e) {
            new AlertDialog.Builder(caller)
                    .setMessage("Can't save file to storage")
                    .setTitle("Import Failed")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            //no need to do anything
                        }
                    }).create().show();
        }
    }


    /**
     * PUBLIC FUNCTIONS
     */
    public String[] combineArrays(String[] a, String[] b) {
        int aLen = a.length;
        int bLen = b.length;
        String[] c = new String[aLen + bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }

    public String[] sortArrayAlphabetically(String[] array) {
        //convert array to List
        List<String> stringList = new ArrayList<>(Arrays.asList(array));

        //sort
        Collections.sort(stringList, String.CASE_INSENSITIVE_ORDER);

        //turn it back into array
        String[] sortedArray = new String[stringList.size()];

        //fill the array with all sorted items
        int i = 0;
        for (String s : stringList) {
            sortedArray[i] = s;
            i++;
        }
        return sortedArray;
    }

    public ArrayList<ShoppingItem> sortItemsAlphabetically(ArrayList<ShoppingItem> itemsArrayList) {
        Collections.sort(itemsArrayList, new Comparator<ShoppingItem>() {
            @Override
            public int compare(ShoppingItem si1, ShoppingItem si2) {
                if (si1 != null && si2 != null) {
                    return si1.getName().compareTo(si2.getName());
                } else {
                    return 0;
                }
            }
        });

        return itemsArrayList;
    }

    public ArrayList<ShoppingItem> sortItemsInOutOfCart(ArrayList<ShoppingItem> itemsArrayList) {
        Collections.sort(itemsArrayList, new Comparator<ShoppingItem>() {
            @Override
            public int compare(ShoppingItem si1, ShoppingItem si2) {
                if (si1 != null && si2 != null) {
                    return Integer.toString(si1.getInCart()).compareTo(Integer.toString(si2.getInCart()));
                }
                return 0;
            }
        });

        return itemsArrayList;
    }

    public void positionToast(Toast toast, View view, Window window, int offsetX, int offsetY) {
        // toasts are positioned relatively to decor view, views relatively to their parents, we have to gather additional data to have a common coordinate system
        Rect rect = new Rect();
        window.getDecorView().getWindowVisibleDisplayFrame(rect);
        // covert anchor view absolute position to a position which is relative to decor view
        int[] viewLocation = new int[2];
        view.getLocationInWindow(viewLocation);
        int viewLeft = viewLocation[0] - rect.left;
        int viewTop = viewLocation[1] - rect.top;

        // measure toast to center it relatively to the anchor view
        DisplayMetrics metrics = new DisplayMetrics();
        window.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(metrics.widthPixels, View.MeasureSpec.UNSPECIFIED);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(metrics.heightPixels, View.MeasureSpec.UNSPECIFIED);
        toast.getView().measure(widthMeasureSpec, heightMeasureSpec);
        int toastWidth = toast.getView().getMeasuredWidth();
        int toastHeight = toast.getView().getMeasuredHeight();

        // compute toast offsets
        int toastX = viewLeft + (view.getWidth() - toastWidth) / 2 + offsetX;

        //original
        //int toastY = viewTop + view.getHeight() + offsetY;

        //my version to position toast just above element
        int toastY = viewTop + view.getHeight() - offsetY - toastHeight;

        toast.setGravity(Gravity.START | Gravity.TOP, toastX, toastY);
    }

    /**
     * **************************************
     * FUNCTIONS RELATED TO CARTABLE ITEMS
     * **************************************
     */
    public int insertShoppingItemIntoDB(ShoppingItem si) {
        return dbh.addShoppingItem(si);
    }

    public ShoppingItem getShoppingItemFromDB(int id) {
        return dbh.getShoppingItem(id);

    }

    public ArrayList<ShoppingItem> getAllShoppingItemsFromDB() {
        return dbh.getAllShoppingItems();
    }

    public void deleteShoppingItem(int id) {
        //remove shoppingItem from DB
        dbh.deleteShoppingItem(id);
    }

    public void deleteAllShoppingItems() {
        dbh.deleteAllShoppingItems();
    }


    public void showNumberOfShoppingItems() {
        int numRem = dbh.getShoppingItemsCount();

        String msg;

        if (numRem == 1) {
            msg = "There is " + numRem + " shoppingItem in the database";
        } else {
            msg = "There are " + numRem + " shoppingItems in the database";
        }

        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public void updateShoppingItem(ShoppingItem si) {
        dbh.updateShoppingItem(si);
    }


    /**
     * **************************************
     * IMPORT-EXPORT
     * **************************************
     */
    public void importDatabase(final Activity caller) {
        permissionRequest = IMPORT_DB;
        int permission = ContextCompat.checkSelfPermission(caller, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(caller, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                new AlertDialog.Builder(caller)
                        .setMessage("Permission to access the storage is required for this app to Import and Export database.")
                        .setTitle("Permission required")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                makePermissionRequest(caller);
                            }
                        }).create().show();
            } else {
                makePermissionRequest(caller);
            }
        } else {
            saveDBfromSD(caller);
        }
    }

    public void exportDatabase(final Activity caller) {
        permissionRequest = EXPORT_DB;
        int permission = ContextCompat.checkSelfPermission(caller, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(caller, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                new AlertDialog.Builder(caller)
                        .setMessage("Permission to access the storage is required for this app to Import and Export database.")
                        .setTitle("Permission required")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                makePermissionRequest(caller);
                            }
                        }).create().show();
            } else {
                makePermissionRequest(caller);
            }
        } else {
            saveDBtoSD(caller);
        }
    }

    private void makePermissionRequest(Activity caller) {
        ActivityCompat.requestPermissions(caller, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults, Activity caller) {
        switch (requestCode) {
            case REQUEST_WRITE_STORAGE: {

                //Permission has been denied by user
                if (grantResults.length == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    new AlertDialog.Builder(caller)
                            .setMessage("It looks like you denied permission to access the storage which is required to Import and Export database.")
                            .setTitle("Permission denied")
                            .setPositiveButton("Close", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //no need to do anything here
                                }
                            }).create().show();
                } else {
                    if (permissionRequest.equals(EXPORT_DB)) {
                        this.saveDBtoSD(caller);
                    } else if (permissionRequest.equals(IMPORT_DB)) {
                        this.saveDBfromSD(caller);
                    }

                }
            }
        }
    }


    /**
     * **************************************
     * FUNCTIONS RELATED TO CUSTOM ITEMS
     * **************************************
     */

    public int insertCustomItemIntoDB(CustomItem ci) {
        return dbh.addCustomItem(ci);
    }

    public CustomItem getCustomItemFromDB(int id) {
        return dbh.getCustomItem(id);

    }

    public ArrayList<CustomItem> getAllCustomItemsFromDB() {
        return dbh.getAllCustomItems();
    }

    public void deleteCustomItem(int id) {
        //remove shoppingItem from DB
        dbh.deleteCustomItem(id);
    }

    public void deleteAllCustomItems() {
        dbh.deleteAllCustomItems();
    }


    public void showNumberOfCustomItems() {
        int numRem = dbh.getCustomItemsCount();

        String msg;

        if (numRem == 1) {
            msg = "There is " + numRem + " shoppingItem in the database";
        } else {
            msg = "There are " + numRem + " shoppingItems in the database";
        }

        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public void updateCustomItem(CustomItem ci) {
        dbh.updateCustomItem(ci);
    }

}
