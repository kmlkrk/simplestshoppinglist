package com.gadgetjudge.simplestshoppinglist;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.gadgetjudge.simplestshoppinglist.helper.OnStartDragListener;
import com.gadgetjudge.simplestshoppinglist.helper.SimpleItemTouchHelperCallback;

import java.util.ArrayList;

public class ActivityMain extends AppCompatActivity implements OnStartDragListener {
	private CoordinatorLayout coordinatorLayout;
    private AutoCompleteTextView autoCompleteTextViewItemName;
    private ImageButton imageButtonAdd;
    private ArrayList<ShoppingItem> itemsInListArray;
	private ArrayList<ShoppingItem> temporaryItems;
    private ShoppingListManager slm;
    private Context applicationContext;
    private String removeSingleItemIntentAction;
    private String editSingleItemIntentAction;
    private String cartSingleItemIntentAction;
    private String importDatabaseIntentAction;
    private IntentFilter removeSingleItemIntentFilter;
    private IntentFilter editSingleItemIntentFilter;
    private IntentFilter cartSingleItemIntentFilter;
    private IntentFilter importDatabaseIntentFilter;
    private BroadcastReceiver customActionsReceiver;
    private String[] predefinedItemsArray;
	private String[] allAvailableItems;
    private ShoppingItem temporaryItem;
	private int temporaryItemIndexInList;
	private View.OnClickListener undoRemoveSingleItemClickListener;
	private View.OnClickListener undoRemoveAllOrInCartItemsClickListener;
	private View.OnClickListener undoSortItemsClickListener;
	private Snackbar undoSnackbar;
	private int snackbarDuration;
    private View adViewRaw;
    private ShoppingItemAdapter shoppingItemAdapter;
    private ItemTouchHelper mItemTouchHelper;
    private TextView emptyListMessage;
    private int toastOffset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.onActivityCreateSetTheme(this);
        setContentView(R.layout.activity_main);


        /* ASSIGN VALUES TO OBJECTS AND VARS */
		coordinatorLayout				= findViewById(R.id.coordinatorLayout);
        autoCompleteTextViewItemName	= findViewById(R.id.autoCompleteTextViewItemName);
        imageButtonAdd					= findViewById(R.id.imageButtonAdd);
        applicationContext				= getApplicationContext();
        slm								= new ShoppingListManager(applicationContext);
        itemsInListArray				= slm.getAllShoppingItemsFromDB();
		removeSingleItemIntentAction	= "com.gadgetjudge.simplestshoppinglist.REMOVE_SINGLE_ITEM";
        editSingleItemIntentAction		= "com.gadgetjudge.simplestshoppinglist.EDIT_SINGLE_ITEM";
        cartSingleItemIntentAction		= "com.gadgetjudge.simplestshoppinglist.CART_SINGLE_ITEM";
        importDatabaseIntentAction		= "com.gadgetjudge.simplestshoppinglist.IMPORT_DATABASE";
        removeSingleItemIntentFilter	= new IntentFilter(removeSingleItemIntentAction);
        editSingleItemIntentFilter		= new IntentFilter(editSingleItemIntentAction);
        cartSingleItemIntentFilter		= new IntentFilter(cartSingleItemIntentAction);
        importDatabaseIntentFilter		= new IntentFilter(importDatabaseIntentAction);
        predefinedItemsArray			= getResources().getStringArray(R.array.shopping_items);
		snackbarDuration				= 5000;//5 seconds
        adViewRaw                       = findViewById(R.id.adView);
        emptyListMessage                = findViewById(R.id.empty_list_view);
        toastOffset						= getResources().getDimensionPixelSize(R.dimen.m_spacing);

        // Keep screen on if user wants to
        boolean keepScreenOn = PreferenceManager.getDefaultSharedPreferences(applicationContext).getBoolean("screen_preference_switch", false);
        if (keepScreenOn) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }

        shoppingItemAdapter = new ShoppingItemAdapter(applicationContext, this, itemsInListArray);
        RecyclerView recyclerView = findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(shoppingItemAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(applicationContext));

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(shoppingItemAdapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(recyclerView);

		createListOfAvailableItems();

        if (BuildConfig.FLAVOR.toLowerCase().equals("free")) {
            // bottom banner
            new Advertisement(applicationContext, "ca-app-pub-8060400006725241/5083256218", adViewRaw);
        }

        //set up broadcast receivers
        customActionsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getExtras() != null) {
                    Bundle intentExtras = intent.getExtras();

                    int position = intentExtras.getInt("item_position");

                    //ACTION FOR REMOVE SINGLE ITEM
                    if (intent.getAction().equals(removeSingleItemIntentAction)) {
                        removeSingleItem(position);
                    }
                    //ACTION FOR EDIT SINGLE ITEM
                    else if (intent.getAction().equals(editSingleItemIntentAction)) {
                        editSingleItem(position);
                    }
                    //ACTION FOR CART SINGLE ITEM
                    else if (intent.getAction().equals(cartSingleItemIntentAction)) {
                        cartSingleItem(position);
                    }
                    //ACTION FOR IMPORT DATABASE
                    else if (intent.getAction().equals(importDatabaseIntentAction)) {
                        importDatabase();
                    }
                }
            }
        };

        //enter key or center button on dpad will do the same as ok button
        autoCompleteTextViewItemName.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    switch (keyCode)
                    {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            addItemToTheList();
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });

		undoRemoveSingleItemClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				undoRemoveSingleItem();
			}
		};

		undoRemoveAllOrInCartItemsClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				undoRemoveAllOrInCartItems();
			}
		};

        undoSortItemsClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				undoSortItems();
			}
		};


        /* SET ON CLICK LISTENERS */

        //add button click listener
        imageButtonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addItemToTheList();
            }
        });

        //item in the list click listener
        autoCompleteTextViewItemName.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                addItemToTheList();
            }
        });

        toggleEmptyListMessage();
    }

	private void createListOfAvailableItems() {
		ArrayList<CustomItem> customItems			= slm.getAllCustomItemsFromDB();
		String[] customItemsArray					= new String[customItems.size()];

		//fill the array with all custom created items
		int i = 0;
		for (CustomItem ci : customItems) {
			customItemsArray[i] = ci.getName();
			i++;
		}

		//add predefined items and custom items to new array and use it to create
		allAvailableItems							= new String[predefinedItemsArray.length + customItemsArray.length];
		allAvailableItems 							= slm.sortArrayAlphabetically(slm.combineArrays(predefinedItemsArray, customItemsArray));
		ArrayAdapter<String> providedItemsAdapter   = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, allAvailableItems);

		//assign adapter with hints to input field
		autoCompleteTextViewItemName.setAdapter(providedItemsAdapter);
	}

    private void addItemToTheList() {
        String itemName = autoCompleteTextViewItemName.getText().toString().trim();

        //make sure that text was entered
        if (!TextUtils.isEmpty(itemName)) {
            if (!isAlreadyInShoppingList(itemName)) {
                ShoppingItem latestItem = new ShoppingItem(itemName, 0);

                //insert item to list as the last item on the list, but before items already added to cart
                try {
                    itemsInListArray.add(findIndexOfFirstItemInCart(), latestItem);
                    slm.insertShoppingItemIntoDB(latestItem);
                    showToast(itemName + " " + getResources().getString(R.string.added_to_the_list));
                } catch (Exception e) {
                    //do nothing
                }
                shoppingItemAdapter.notifyDataSetChanged();

                //check if item is already in the list of known items, if not add
                if (!isInKnownItemsList(itemName)) {
                    CustomItem ci = new CustomItem(itemName);
                    slm.insertCustomItemIntoDB(ci);
                    showToast(itemName + " " + getResources().getString(R.string.added_to_known_items));

                    createListOfAvailableItems();
                }

                toggleEmptyListMessage();
            } else {
                showToast(itemName + " " + getResources().getString(R.string.item_already_in_list));
            }

            autoCompleteTextViewItemName.setText("");
        }
        else {
			showToast(getResources().getString(R.string.empty_input_error_message));
        }
    }

    private boolean isInKnownItemsList(String itemName) {
        boolean found = false;
        for (String existingItem: allAvailableItems) {
            if (existingItem.trim().toLowerCase().equals(itemName.trim().toLowerCase())) {
                found = true;
                break;
            }
        }

        return found;
    }

    private boolean isAlreadyInShoppingList(String itemName) {
        boolean found = false;
        for (ShoppingItem existingItem: itemsInListArray) {
            if (existingItem != null && existingItem.getName().trim().toLowerCase().equals(itemName.trim().toLowerCase())) {
                found = true;
                break;
            }
        }

        return found;
    }

    private int findIndexOfFirstItemInCart() {
        int index = 0;
        for (int i = 0; i < itemsInListArray.size() - 1; i++) {
            //if element marked as in cart found stop the loop
            if (itemsInListArray.get(i) != null) {
                if (itemsInListArray.get(i).getInCart() == 1) {
                    break;
                }
//              only increase index if elm is not null (because last elm is ad and always null)
                index++;
            }
        }
        return index;
    }

    private void undoRemoveSingleItem() {
        try {
            itemsInListArray.add(temporaryItemIndexInList, temporaryItem);
        } catch (Exception e) {
            //do nothing
        }
        shoppingItemAdapter.notifyDataSetChanged();

        showToast(temporaryItem.getName() + " " + getResources().getString(R.string.restored));
    }

    private void hideUndoSnackbar() {
		//snack might not have been created
		try {
			undoSnackbar.dismiss();
		}
		catch (Exception e) {
			//do nothing
		}
    }

    private void removeSingleItem(int position) {
        ShoppingItem si = itemsInListArray.get(position);
        temporaryItem = si;

		removeSingleItemWorker(position);
		showUndoSnackbar(si.getName() + " " + getResources().getString(R.string.removed_from_the_list), snackbarDuration, getResources().getString(R.string.undo), undoRemoveSingleItemClickListener);
		toggleEmptyListMessage();
    }

    private void removeSingleItemWorker(int position) {
        temporaryItemIndexInList = position;
        itemsInListArray.remove(position);
        shoppingItemAdapter.notifyDataSetChanged();
    }


    private void editSingleItem(final int position) {
        final ShoppingItem si = itemsInListArray.get(position);

        //check if field is empty, if not ask user if they want to replace current text with edited item
        //otherwise place text in the field

        if (!TextUtils.isEmpty(autoCompleteTextViewItemName.getText())) {
            //alert user that field not empty
            final AlertDialog.Builder promptDialog = new AlertDialog.Builder(this);
            promptDialog
                    //.setMessage(getResources().getString(R.string.item_name_not_empty_warning))
                    .setMessage("You already entered \'"+ autoCompleteTextViewItemName.getText() +"\' but didn't save it yet. Do you want to save it before editing \'"+ si.getName() +"\'")
                    .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            addItemToTheList();
                            editSingleItemWorker(position);
                        }
                    })
                    .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            editSingleItemWorker(position);
                        }
                    }).create().show();
        }
        //input field was empty, we can proceed with editing
        else {
            editSingleItemWorker(position);
        }
    }

    private void editSingleItemWorker(int position) {
        ShoppingItem si = itemsInListArray.get(position);
        autoCompleteTextViewItemName.setText(si.getName());

        //move cursor to the end of text
        autoCompleteTextViewItemName.setSelection(autoCompleteTextViewItemName.length());

        removeSingleItemWorker(position);
    }

    private void cartSingleItem(int position) {
        ShoppingItem si = itemsInListArray.get(position);

        if (si != null) {
            //LOGIC FOR MOVING TO CART
            if (si.getInCart() == 0) {
                si.setInCart(1);

                //remove it first
                itemsInListArray.remove(position);

                //insert at the end
                itemsInListArray.add(itemsInListArray.size() -1, si); /* last item is dummy to make room for ad hence the -1 */
                shoppingItemAdapter.notifyDataSetChanged();

                showToast(" \'" + si.getName() + "\' " + getResources().getString(R.string.moved_to_cart));
            }
            //LOGIC FOR MOVING OUT OF CART
            else {
                si.setInCart(0);

                //remove it first
                itemsInListArray.remove(position);

                //insert at the end but before carted items
                try {
                    itemsInListArray.add(findIndexOfFirstItemInCart(), si);
                } catch (Exception e) {
                    //do nothing
                }
                shoppingItemAdapter.notifyDataSetChanged();

                showToast(" \'" + si.getName() + "\' " + getResources().getString(R.string.removed_from_cart));
            }
        }
    }

	private void undoRemoveAllOrInCartItems() {
        itemsInListArray.addAll(findIndexOfFirstItemInCart(), temporaryItems);
        shoppingItemAdapter.notifyDataSetChanged();
        saveAllToDB();
	}

    private void undoSortItems() {
        slm.deleteAllShoppingItems();
        itemsInListArray.clear();
//      do not add last item because it is an ad
        itemsInListArray.addAll(findIndexOfFirstItemInCart(), temporaryItems);
        shoppingItemAdapter.notifyDataSetChanged();
        saveAllToDB();
    }
    private void removeAllItems() {
        //check if list not empty (skip last item which is ad)
        if (itemsInListArray.size() -1 > 0) {

			temporaryItems = new ArrayList<>();
			int lastShoppingItemIndex = itemsInListArray.size()-1;
			temporaryItems.addAll(itemsInListArray.subList(0, lastShoppingItemIndex));
			itemsInListArray.subList(0, lastShoppingItemIndex).clear();

			shoppingItemAdapter.notifyDataSetChanged();

			//delete al items from db
			slm.deleteAllShoppingItems();

			//show empty list message
			toggleEmptyListMessage();

            saveAllToDB();
			showUndoSnackbar(getResources().getString(R.string.items_removed), snackbarDuration, getResources().getString(R.string.undo), undoRemoveAllOrInCartItemsClickListener);
        }
        //list is empty, display toast
        else {
            showToast(getResources().getString(R.string.no_items_in_list));
        }
    }

	private void showUndoSnackbar(String text, int duration, String actionText, View.OnClickListener listener) {
		undoSnackbar = Snackbar.make(coordinatorLayout, text, duration);
		undoSnackbar.setAction(actionText, listener);
		undoSnackbar.setActionTextColor(ContextCompat.getColor(applicationContext, R.color.accent));

		View undoDeleteSnackView = undoSnackbar.getView();
		TextView textView = undoDeleteSnackView.findViewById(android.support.design.R.id.snackbar_text);
		textView.setPadding(4, 0, 0, 0);

		undoSnackbar.show();
	}

    private void removeItemsInCart() {
        //check if there are items in cart if yes show prompt otherwise show toast
        if (isAnythingInCart()) {

			temporaryItems = new ArrayList<>();

			//delete cart items from db
			//this MUST run backwards because with every removal adapter count gets smaller so if we
			//run the loop in regular direction and remove elm at index 5 at the next iteration
			//the adapter may only have 5 items left so that index 5 will be out of bounds

			for (int i = itemsInListArray.size() -1; i >= 0; i--) {

				if (itemsInListArray.get(i) != null && itemsInListArray.get(i).getInCart() == 1) {
					//first add all items to be removed to tmp array
					temporaryItems.add(itemsInListArray.get(i));

					//remove from list
					itemsInListArray.remove(itemsInListArray.get(i));
				}
			}

			shoppingItemAdapter.notifyDataSetChanged();

			//show empty list message
			toggleEmptyListMessage();

			showUndoSnackbar(getResources().getString(R.string.cart_items_removed), snackbarDuration, getResources().getString(R.string.undo), undoRemoveAllOrInCartItemsClickListener);
        }
        //show toast that there are no items moved to cart yet
        else {
            showToast(getResources().getString(R.string.no_items_in_cart_yet));
        }
    }

    private void sortItems() {
        //check if list not empty
        if (itemsInListArray.size() -1 > 0) {

            //backup current list for UNDO functionality
            temporaryItems = new ArrayList<>();
            int lastShoppingItemIndex = itemsInListArray.size()-1;
            temporaryItems.addAll(itemsInListArray.subList(0, lastShoppingItemIndex));

            //get sorted list
            itemsInListArray = slm.sortItemsAlphabetically(itemsInListArray);
            itemsInListArray = slm.sortItemsInOutOfCart(itemsInListArray);

            //show updated result
            shoppingItemAdapter.notifyDataSetChanged();

            saveAllToDB();
            showUndoSnackbar(getResources().getString(R.string.items_sorted), snackbarDuration, getResources().getString(R.string.undo), undoSortItemsClickListener);
        }
        //list is empty, display toast
        else {
            showToast(getResources().getString(R.string.no_items_in_list));
        }
    }

	private void startEditCustomItemsActivity() {
		Intent editCustomItemsIntent = new Intent(ActivityMain.this, ActivityCustomItems.class);
		editCustomItemsIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		ActivityMain.this.startActivity(editCustomItemsIntent);
	}

    private void sendList() {

        //make sure that list is not empty
        if (itemsInListArray.size() -1 > 0) {
            //build the shareable list string
            String shareableList = getResources().getString(R.string.please_buy);
            int numberOfShareableItems = 0;

            for (int i = 0; i < itemsInListArray.size() -1; i++) {
                if (itemsInListArray.get(i) != null && itemsInListArray.get(i).getInCart() == 0) {
                    //increment number of shareable items
                    numberOfShareableItems++;
                    shareableList = shareableList.concat("\n- " + itemsInListArray.get(i).getName());
                }
            }

            //there is at least one item to share
            if (numberOfShareableItems > 0) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, shareableList);
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, getResources().getString(R.string.send_list_using)));
            }
            else {
                showToast(getResources().getString(R.string.list_empty_or_all_items_in_cart));
            }
        }
        //list is empty
        else {
            showToast(getResources().getString(R.string.list_empty_message));
        }
    }


    private void toggleEmptyListMessage() {
        if (itemsInListArray.size() -1 == 0) {
            emptyListMessage.setVisibility(View.VISIBLE);
        } else {
            emptyListMessage.setVisibility(View.GONE);
        }
    }

    private boolean isAnythingInCart() {
        boolean itemInCart = false;

        for (int i = 0; i < itemsInListArray.size() -1; i++) {
            if (itemsInListArray.get(i) != null && itemsInListArray.get(i).getInCart() == 1) {
                itemInCart = true;
                break;
            }
        }

        return itemInCart;
    }

    private void showToast(String text) {
		//hide Snackbar if visible
		hideUndoSnackbar();

		//show Toast
        Toast toast = Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT);
        slm.positionToast(toast, adViewRaw, getWindow(), 0, toastOffset);
        toast.show();
    }

    private void upgradeToPro() {
        final String appName = "com.gadgetjudge.simplestshoppinglistpro";
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id="+appName)));
        }
    }

    private void privacyPolicy() {
        final String policyAddress = "https://sites.google.com/view/gadgetjudge/ssl-privacy-policy";
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(policyAddress)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(policyAddress)));
        }
    }

    private void showSettings() {
        Intent settingsIntent = new Intent(applicationContext, ActivitySettings.class);
        settingsIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(settingsIntent);
    }

    private void saveAllToDB() {
        slm.deleteAllShoppingItems();
        for (ShoppingItem si: itemsInListArray) {
            slm.insertShoppingItemIntoDB(si);
        }
    }

    private void importDatabase() {
        itemsInListArray.clear();
        itemsInListArray.addAll(findIndexOfFirstItemInCart(), slm.getAllShoppingItemsFromDB());
        this.recreate();
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_main_generic, menu);

		//free version has upgrade button
		if (BuildConfig.FLAVOR.toLowerCase().equals("free")) {
		    getMenuInflater().inflate(R.menu.menu_activity_main_free, menu);
		}


		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.menu_remove_all_items) {
			removeAllItems();
			return true;
		}
		else if (id == R.id.menu_remove_cart_items) {
			removeItemsInCart();
			return true;
		}
		else if (id == R.id.menu_send_list) {
			sendList();
			return true;
		}
		else if (id == R.id.menu_edit_custom_items) {
			startEditCustomItemsActivity();
		}
        else if (id == R.id.menu_sort_items) {
            sortItems();
        }
        else if (id == R.id.menu_settings) {
            showSettings();
        }
		//this is only avaialbale in the free version
        else if (id == R.id.menu_privacy_policy) {
            privacyPolicy();
        }
        else if (id == R.id.menu_upgrade_to_pro) {
			upgradeToPro();
		}
        else if (id == R.id.menu_import) {
            slm.importDatabase(this);
        }
        else if (id == R.id.menu_export) {
            slm.exportDatabase(this);
        }

		return super.onOptionsItemSelected(item);
	}

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        slm.onRequestPermissionsResult(requestCode, permissions, grantResults, ActivityMain.this);
    }

    @Override
    protected void onResume() {
        super.onResume();

		//recreate list of available items - it might have been modified in manager
		createListOfAvailableItems();

        this.registerReceiver(customActionsReceiver, removeSingleItemIntentFilter);
        this.registerReceiver(customActionsReceiver, editSingleItemIntentFilter);
        this.registerReceiver(customActionsReceiver, cartSingleItemIntentFilter);
        this.registerReceiver(customActionsReceiver, importDatabaseIntentFilter);
    }

    @Override
    public void onPause() {
        super.onPause();

        saveAllToDB();

        unregisterReceiver(customActionsReceiver);
    }
}
