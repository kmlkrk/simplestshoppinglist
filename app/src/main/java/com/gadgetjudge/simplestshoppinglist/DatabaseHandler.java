package com.gadgetjudge.simplestshoppinglist;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;


public class DatabaseHandler extends SQLiteOpenHelper {
	
	private static DatabaseHandler sInstance = null;
	
	//database version
	private static final int DATABASE_VERSION = 2;
	
	//database name
	public static final String DATABASE_NAME = "shoppingListDB";
	
	//shopping items table name
	private static final String TABLE_SHOPPING_ITEMS = "shoppingItems";

	//custom items table name
	private static final String TABLE_CUSTOM_ITEMS = "customItems";

	//shopping items table column names
	private static final String KEY_ID = "id";
	private static final String KEY_NAME = "name";
	private static final String KEY_IN_CART = "inCart";

	//custom items table column names
	private static final String KEY_CI_ID = "id";
	private static final String KEY_CI_NAME = "name";

	//SQL QUERIES
	private static final String CREATE_REMINDERS_TABLE = "CREATE TABLE " + TABLE_SHOPPING_ITEMS + "("
			+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT,"
			+ KEY_IN_CART + " INTEGER" + ")";

	private static final String CREATE_CUSTOM_ITEMS_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_CUSTOM_ITEMS + "("+ KEY_ID
			+ " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT" + ")";

	// Use the application context, which will ensure that you 
    // don't accidentally leak an Activity's context.
    // See this article for more information: http://bit.ly/6LRzfx
	public static DatabaseHandler getInstance(Context context) {
		if (sInstance == null) {
			sInstance = new DatabaseHandler(context.getApplicationContext());
		}
		return sInstance;
	}

	//constructor
	private DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	//create database and table
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_REMINDERS_TABLE);
		db.execSQL(CREATE_CUSTOM_ITEMS_TABLE);
	}
	
	//upgrade database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		if (oldVersion == 1) {
			upgradeFrom1to2(db);
			oldVersion = 2;
		}
		
		if (oldVersion == 2) {
			upgradeFrom2to3(db);
			oldVersion = 3;
		}


	}
	
	private void upgradeFrom1to2(SQLiteDatabase db){
		//EXAMPLE FROM REMINDER APP
		//String ALTER_REMINDERS_TABLE_ADD_COLUMN_RECURRENCE 			= "ALTER TABLE " + TABLE_SHOPPING_ITEMS + " ADD COLUMN " + KEY_RECURRENCE_POSITION + " INTEGER DEFAULT 0";
		//String ALTER_REMINDERS_TABLE_ADD_COLUMN_ORIGINAL_TIMESTAMP 	= "ALTER TABLE " + TABLE_SHOPPING_ITEMS + " ADD COLUMN " + KEY_ORIGINAL_TIMESTAMP + " INTEGER DEFAULT 0";
		//db.execSQL(ALTER_REMINDERS_TABLE_ADD_COLUMN_RECURRENCE);
		//db.execSQL(ALTER_REMINDERS_TABLE_ADD_COLUMN_ORIGINAL_TIMESTAMP);

		db.execSQL(CREATE_CUSTOM_ITEMS_TABLE);
	}
	
	private void upgradeFrom2to3(SQLiteDatabase db){
		//for future use
	}


	/**
	* **************************************
	* FUNCTIONS RELATED TO CARTABLE ITEMS
	* **************************************
	*/

	//add new item
	public int addShoppingItem(ShoppingItem shoppingItem) {
		int insertedRowId = 0;
		if (shoppingItem != null) {
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(KEY_NAME, shoppingItem.getName());
			values.put(KEY_IN_CART, shoppingItem.getInCart());

			//create long that will be returned

			//insert row
			insertedRowId = (int) db.insert(TABLE_SHOPPING_ITEMS, null, values);
			db.close();
		}
		return insertedRowId;
	}
	
	//get single item
	public ShoppingItem getShoppingItem(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		
		Cursor cursor = db.query(TABLE_SHOPPING_ITEMS, new String[] { KEY_ID, KEY_NAME,
				KEY_IN_CART }, KEY_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);

		if(cursor != null && cursor.moveToFirst()) {
			/*0=id
             *1=name
             *2=is in cart
             */

                ShoppingItem shoppingItem = new ShoppingItem(Integer.parseInt(cursor.getString(0)),
                    cursor.getString(1), Integer.parseInt(cursor.getString(2)));

            cursor.close();
            db.close();

            return shoppingItem;
		}
        //prevent force close on java null pointer exception
        return new ShoppingItem(0, "", 0);
	} 
	
	//get all shopping items
	public ArrayList<ShoppingItem> getAllShoppingItems() {
		ArrayList<ShoppingItem> shoppingItemsList = new ArrayList<>();
		
		//select all query, SORT IT FIRST BY STATUS (enabled first) then by DATE
		String selectQuery = "SELECT * FROM " + TABLE_SHOPPING_ITEMS + " ORDER BY " + KEY_IN_CART + " ASC, " + KEY_ID + " ASC";
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		
		//looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				
				ShoppingItem shoppingItem = new ShoppingItem(Integer.parseInt(cursor.getString(0)),
						cursor.getString(1), Integer.parseInt(cursor.getString(2)));
				
				//insert item into the list
                shoppingItemsList.add(shoppingItem);
			}
			while (cursor.moveToNext());
		}
		
		cursor.close();
		db.close();
		
		return shoppingItemsList;
	}
	
	//get shopping items count
	public int getShoppingItemsCount() {
		String countQuery = "SELECT * FROM " + TABLE_SHOPPING_ITEMS;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		
		int count = cursor.getCount();
		
		cursor.close();
		db.close();
		
		return count;
	}
	
	//update single shopping item
	public int updateShoppingItem(ShoppingItem shoppingItem) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues values = new ContentValues();
		values.put(KEY_NAME, shoppingItem.getName());
		values.put(KEY_IN_CART, shoppingItem.getInCart());
		
		
		//update row
		int affectedRows = db.update(TABLE_SHOPPING_ITEMS, values, KEY_ID + "=?",
				new String[] { String.valueOf(shoppingItem.getId()) });
		
		db.close();
		
		return affectedRows;
	}
	
	//delete single shopping item by passing id
	public void deleteShoppingItem(int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_SHOPPING_ITEMS, KEY_ID + "=?",
				new String[] { String.valueOf(id) });
		db.close();
	}

    //delete all shopping items
    public void deleteAllShoppingItems() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SHOPPING_ITEMS, null, null);
        db.close();
    }






	/**
	* **************************************
	* FUNCTIONS RELATED TO CUSTOM ITEMS
	* **************************************
 	*/

	//add new item
	public int addCustomItem(CustomItem customItem) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_CI_NAME, customItem.getName());

		//create long that will be returned
		int insertedRowId;

		//insert row
		insertedRowId = (int) db.insert(TABLE_CUSTOM_ITEMS, null, values);
		db.close();

		return insertedRowId;
	}

	//get single item
	public CustomItem getCustomItem(int id) {
		SQLiteDatabase db = this.getReadableDatabase();

		Cursor cursor = db.query(TABLE_CUSTOM_ITEMS, new String[] { KEY_CI_ID, KEY_CI_NAME }, KEY_CI_ID + "=?",
				new String[] { String.valueOf(id) }, null, null, null, null);

		if(cursor != null && cursor.moveToFirst()) {
			/*0=id
             *1=name
             */

			CustomItem customItem = new CustomItem(Integer.parseInt(cursor.getString(0)),
					cursor.getString(1));

			cursor.close();
			db.close();

			return customItem;
		}
		//prevent force close on java null pointer exception
		return new CustomItem(0, "");
	}

	//get all custom items
	public ArrayList<CustomItem> getAllCustomItems() {
		ArrayList<CustomItem> customItemsList = new ArrayList<>();

		//select all query, SORT IT FIRST BY STATUS (enabled first) then by DATE
		String selectQuery = "SELECT * FROM " + TABLE_CUSTOM_ITEMS + " ORDER BY " + KEY_CI_NAME + " ASC";

		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		//looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {

				CustomItem customItem = new CustomItem(Integer.parseInt(cursor.getString(0)),
						cursor.getString(1));

				//insert item into the list
				customItemsList.add(customItem);
			}
			while (cursor.moveToNext());
		}

		cursor.close();
		db.close();

		return customItemsList;
	}

	//get custom items count
	public int getCustomItemsCount() {
		String countQuery = "SELECT * FROM " + TABLE_CUSTOM_ITEMS;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);

		int count = cursor.getCount();

		cursor.close();
		db.close();

		return count;
	}

	//update single custom item
	public int updateCustomItem(CustomItem customItem) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_CI_NAME, customItem.getName());


		//update row
		int affectedRows = db.update(TABLE_CUSTOM_ITEMS, values, KEY_CI_ID + "=?",
				new String[] { String.valueOf(customItem.getId()) });

		db.close();

		return affectedRows;
	}

	//delete single custom item by passing id
	public void deleteCustomItem(int id) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_CUSTOM_ITEMS, KEY_CI_ID + "=?",
				new String[] { String.valueOf(id) });
		db.close();
	}

	//delete all custom items
	public void deleteAllCustomItems() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_CUSTOM_ITEMS, null, null);
		db.close();
	}

}
