package com.gadgetjudge.simplestshoppinglist;

/**
 * Created by kamil on 3/8/15.
 */
public class CustomItem {

    private int id;
    private String name;

    public CustomItem(String name) {
        this.name = name;
    }

    public CustomItem(int id, String name) {
        this.id = id;
        this.name = name;
    }

    //GETTERS
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    //SETTERS
    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CustomItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
