package com.gadgetjudge.simplestshoppinglist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

public class ActivitySettings extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
	final String KEY_THEME = "theme_preference_switch";
	final String KEY_SCREEN = "screen_preference_switch";
	boolean isCurrentlyDarkThemeUsed;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Utils.onActivityCreateSetTheme(this);
		super.onCreate(savedInstanceState);

		getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();

		SharedPreferences userSettings = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		userSettings.registerOnSharedPreferenceChangeListener(this);

		isCurrentlyDarkThemeUsed = userSettings.getBoolean(KEY_THEME, false);
	}
	
	@Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (key.equals(KEY_THEME)) {
			final boolean useDarkTheme = sharedPreferences.getBoolean(KEY_THEME, false);

			if (isCurrentlyDarkThemeUsed != useDarkTheme) {
				Intent intent = getIntent();

				//close all activities so that new theme can be applied
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);//prevents multiple settings activities open when user selects theme few times in a row

				//add correct values to the intent
				intent.putExtra("theme_changed", true);

				finish();
				startActivity(intent);
			}
		} else if (key.equals(KEY_SCREEN)) {
			Intent intent = getIntent();
			//close all activities so that new theme can be applied
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);//prevents multiple settings activities open when user selects screen few times in a row

			//add correct values to the intent
			intent.putExtra("screen_changed", true);

			finish();
			startActivity(intent);

		}
    }

    @Override
    public void onBackPressed() {
		super.onBackPressed();

		boolean themeChanged = getIntent().getBooleanExtra("theme_changed", false);
		boolean screenChanged = getIntent().getBooleanExtra("screen_changed", false);
        if (themeChanged || screenChanged) {

            Intent intent = new Intent(this, ActivityMain.class);
            startActivity(intent);

            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            finish();
        }
    }
}